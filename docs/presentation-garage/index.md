# Présentation du Garage Numérique

Ces documents présentent le fonctionnement de l'association.

Vous pouvez consulter le [réglement intérieur](reglement_interieur.md).  

Vous pouvez vous reporter aux [procédures en vigueur](procedures.md).  

Vous pouvez accéder à la description des [formations DevOps](formation).
