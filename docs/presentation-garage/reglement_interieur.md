# Réglement intérieur

v. 0.0.2

## Introduction

Le présent réglement permet de définir les conditions de fonctionnement des activités au sein de l'association.

Il est organisé en 3 parties:  
1. [Dispositions générales](#dispositions-generales)  
2. [Dispositions pour l'équipe](#dispositions-pour-lequipe)  
3. [Dispositions spécial Covid](#dispositions-special-covid)  


## Dispositions générales

### Introduction 

Les dispositions générales concernent :  
  - l'[évolution du présent réglement](#historique-des-versions)  
  - les [destinataires](#destinataires) du présent réglement  
  - les [horaires d'accès aux locaux](#acces-aux-locaux)  
  - les [canaux de communication au sein de l'assocation](#contacter-lassociation)  
  - les [modes d'application du réglement intérieur](#application-du-reglement)  
  - les [comportements et valeurs à respecter](#valeurs-et-comportements)  
  - les [sanctions](#sanctions)  

Des dispositions spécifiques peuvent s'ajouter à ces dispositions générales

### Historique des versions

Le présent réglement, étant amené à évoluer au cours du temps, est marqué d'un numéro de version qui permet d'assurer un suivi strict des changements.

La numérotation des versions suit le schéma SemVer ( format **`Maj.min.Patch`**, exemple: `1.12.23`) :   
- le 1er numéro correspond à un changement majeur dans le règlement  
- le 2eme numéro correspond à un changement mineur  
- le 3eme numéro correspond à un correctif qui affecte la forme et pas le fond  

Le suivi des versions est associé à un suivi des changements explicités dans le fichier [CHANGELOG](reglement_interieur_changelog). 

### Destinataires

Le présent réglement s'applique à l'ensemble des membres du Garage Numérique:  
  - salariés,  
  - administrateurs,  
  - bénévoles,  
  - volontaires sous la forme d'un contrat d'engagement (service civique, garantie jeunes, etc),  
  - étudiants,  
  - stagiaires,  
  - adhérents,  
  - etc,   
ainsi qu'aux usagers qui en sollicitent les services. 

### Accès aux locaux

Les locaux du Garage Numérique sont susceptibles d'être ouverts du lundi au samedi de 9h à 19h30.  
En dehors de ces horaires, aucun membre ne peut accéder aux locaux sauf autorisation exceptionnelle du Conseil d'Administration.

### Contacter l'association

Les membres de l'association sont joignables, par ordre de préférence:  

| du plus au moins utilisé | si vous êtes membres de l'équipe (cf #destinataires) | Si vous êtes extérieur à l'association |
|-----------|---------------|-----------|
| 1 | par Element sur le réseau Matrix         | par téléphone / sms, au numéro d'accueil |
| 2 | par mail à contact@legaragenumerique.fr     |  par mail à contact@legaragenumerique.fr |
| 3 | par téléphone / sms, au numéro d'accueil  | par Twitter / FB / whattsappp / insta / snap |

### Application du réglement

Chaque mise à jour du présent réglement est signifiée à l'ensemble des membres sur le canal général du réseau Matrix.  

Les membres ont 48h pour prendre connaissance des nouvelles dispositions et s'y conforter. 

Les remarques doivent être adressées par mail à `contact@legaragenumerique.fr` à l'attention du président.

Sauf lorsque précisé, les dispositions du présent réglement font effet dès parution.

Lorsque les modifications du règlement entraînent une modification des conditions de travail, alors les conditions d'approbation et de mise en éxecution de ces modifications sont subordonnées aux dispositions en vigueur dans le code du Travail et la Convention Collective.


### Valeurs et comportements

Le Garage Numérique est une association d'Éducation Populaire dont le système de valeurs intègre:  
- la non-discrimination des personnes en fonction de leur appartenance religieuse, de genre,  
- la bienveillance  

Aussi, aucun comportement de violence physique ou verbale, ainsi qu'aucune forme d'intimidation ou de menace ne sera tolérée, que ce soit dans les locaux, sur les réseaux, entre membres ou envers les usagers.  

Tout manquement sera fortement sanctionné.  
      
### Sanctions  

L'association peut être amenée à prononcer des sanctions à l'égard de ses membres, qu'ils soient membres actifs ou usagers.  

Quelle que soit la faute commise et la sanction envisagée, celle-ci doit toujours être déterminée de manière impartiale, après avoir pu entendre le point de vue de chacun des protagonistes, ainsi que de manière proportionnée à la gravité de la faute, en considération des circonstances d'éxecution de la faute.

L'incident, le mode de résolution de l'incident, le mode de détermination de la sanction, ainsi que les conditions d'éxecution de la sanction doivent être consignées et transmises au Conseil d'Administration.      

## Dispositions pour l'équipe

### Horaires de Travail

Les horaires de travail au sein de l'association s'étendent du lundi au samedi, de 9h00 à 19h00.

La journée de travail standard commence à 9h30 à termine à 18h, avec une pause méridienne entre 12h30 et 14h.

Cependant ces horaires peuvent être adaptés dans les limites des horaires pré-cités ( 9h-19h ).

Les journées de travail doivent s'étendre au maximum sur une amplitude de 9h, comprenant au maximum 8h de travail effectif.

Les semaines comportent toujours deux jours de repos consécutifs, dont le dimanche. 

L'adaptation de ces horaires peut être nécessitée selon:  
- les postes de travail,   
- les volumes de charge de travail (projet en cours, échéance)  

La mise en place des plannings doit se faire deux semaines en avance, et doit être concertée. Doivent être pris en compte au mieux les dispositions de chacun afin que tous bénéficient d'un environnement de travail favorable.

### Accès aux locaux

L'accès aux locaux se fait avec l'autorisation d'un des membres ayant la clé.

Aucun accès n'est possible en dehors des plages horaires prévues, sauf autorisation exceptionnelle du Conseil d'Administration.

Tout manquement à ces consignes pourra être sanctionné.

### Report des actions

#### Report dans l'agenda  

**Chaque accès au local doit systématiquement être enregistré dans l'agenda Nextcloud.**

Un événement doit être créé pour chaque créneau (ex: 1 matin, 1 après-midi), et chaque membre d'équipe valide sa présence dans l'agenda Nextcloud.


#### Report dans le journal de bord  

Le journal de bord est un canal de discussion privée sur le réseau Matrix, qui permet de logger l'activité du Garage Numérique. En utilisant l'API, il est facile de produire des compilations spécifiques, par exemple, le journal de bord d'un stagiaire.

Il est demandé à tous les membres d'équipe de participer à la réalisation de ce journal de bord. Il n'y a pas d'exigence quant à la quantité à écrire, en revanche il est exigé un apport quotidien, ne serait-ce que pour savoir sur quel projet chacun à travaillé.

Pour les membres qui participent à l'accueil des usagers, il est demandé de penser à noter dans le journal de bord les passages d'usagers et leur demande.  
Le log des passages à l'accueil doit comporter le mot-clé `#accueil`.

Quant un incident se déroule au sein des locaux de l'association, ou dans le cadre d'une activité à l'extérieur, il est **indispensable** de noter précisément le déroulement de l'incident dans le journal de bord, avec le mot-clé `#incident`.

### Travail à réaliser

#### Présentation du travail à réaliser 

C'est l'application Nextcloud qui permet à chacun de visualiser le travail à réaliser :  
  * le module `Agenda` attribue à chacun des créneaux de présence pour les différentes tâches  
  * les modules `Tâches` et `Deck` permettent tous les deux d'accéder aux listes de tâches ponctuelles

#### Types de tâches

Chacun est en charge de plusieurs types de tâches:  
  * la mission principale:  
    elle est différente pour chacun, elle représente la majorité du temps de travail, elle correspond aux compétences de chacun ;   
  * le projet:  
    chacun travaille sur un projet au long cours, qui doit lui permettre de monter en compétences et de produire des outils pour l'association ;  
  * les missions ponctuelles:   
    dans les limites de son champ de compétences, chacun est responsable de missions ponctuelles, de courte durée, qui diffèrent et de la mission principale et du projet ;   
  * les tâches polyvalentes:   
    certaines tâches demandent une participation de tous : accueil, entretien des locaux, réunions d'équipes ;  
  * la formation:  
    des heures réservées à la formation du salarié, au sein ou en dehors de son domaine de compétences, selon son projet personnel.

#### Répartition du travail à réaliser

Prenons l'exemple d'un technicien de maintenance, travaillant 35h / semaine, soit environ 150 heures / mois.

| Type de tâche | Description de la tâche  | Répartition  | Volume hebdomadaire |
|---------------|--------------------------|--------------|---------------------|
| Mission principale | Réparer les ordinateurs | 40 %   |   15 heures  |
| Projet   |  Développer un outil de maintenance | 20 % |  7 heures |  
| Mission |  s'occuper de la récupération et du reconditionnement d'un lot d'ordis | 20 % | 7 heures |
| Polyvalence | Ménage et réunion d'équipe  | 10 % |  3 heures |
| Formation | Préparer une certif Linux | 10 % |   3 heures | 

La répartition présentée ici est approximative et est amené à varier sensiblement en fonction du poste de chacun, des évolutions programmées du poste, de la charge de travail globale.

## Dispositions spécial Covid

### Introduction

En raison de l'état de pandémie Covid-19, le fonctionnement de l'association doît être modifié significativement afin d'assurer une continuité du service et de protéger les membres et usagers de l'association.

Cette partie du réglement intérieur se présente en trois parties:  
- [Rappels sur le virus et la maladie](#rappels-sur-le-virus-et-la-maladie)  
- [Dispositions sanitaires](#dispositions-sanitaires)  
- [Organisation du travail](#organisation-du-travail)  

### Rappels sur le virus et la maladie

Le virus se transmet principalement par voie aérienne, grâce à des micro-goutelettes et des particules en suspension que nous excrêtons quand nous toussons, quand nous parlons, quand nous respirons.   
Le virus peut aussi se transmettre en touchant une surface contaminée, puis en touchant notre visage (c'est un mouvement involontaire que l'on fait plusieurs dizaines de fois par jour).  

Le virus est hautement contagieux. Dans une pièce fermée de la taille du local du Garage Numérique, une personne infectée qui ne porte pas de masque en contamine 5 autres en 1 heure.

Une personne contaminée peut être responsable de la transmission du virus à partir de 24 à 48h après la contamination, et ce pendant une semaine, sans ressentir aucun symptôme.  

Pour les autres, les symptômes vont apparaître généralement au cours de la 1ère semaine après la contamination, et la personne est alors contaminante pendant environ 15 jours. 

Au bout d'une à deux semaines, les personnes guérissent spontanément, ou leur état s'aggrave et elles doivent bénéficier souvent d'une assistance respiratoire.  

Pour certains, leur état peut s'aggraver encore malgré l'assistance respiratoire et elles doivent être placées en réanimation. La réanimation peut durer jusqu'à 12 semaines, et entraîner de nombreuses séquelles handicapantes.  

Si la personne est trop âgée pour supporter les techniques de réanimation, ou si il n'y a plus de place dans les hôpitaux, la personne malade dont l'état se dégrade subitement est condamnée à une mort rapide.  

Certains jeunes aussi développent des formes graves de la maladie, et de nombreuses personnes qui ont été contaminé mais n'ont pas eu de symptômes ou des symptômes légers ont par la suite des difficultés respiratoires, cardiaques, immunitaires, pendant plusieurs mois. 

### Dispositions sanitaires

#### Port du masque

Le port du masque est la principale barrière à notre disposition pour ralentir la propagation de l'épidémie.   

Le port du masque est strictement obligatoire au sein des locaux du Garage Numérique, pour toute personne, quelque soit son statut au sein de l'association. 

Les membres de l'équipe sont priés de prendre un masque neuf à l'accueil en entrant APRÈS s'être lavés les mains.

#### Hygiène des mains

En entrant dans les locaux, on est prié de se laver les mains à l'eau et au savon dans les sanitaires.  

Du gel hydro-alcoolique est aussi mis à disposition à l'accueil.  

#### Capacité d'accueil du local

Les locaux sont strictement limités :  
* à la présence de 10 personnes dans le grand local (dont 2 usagers, soit 8 membres de l'équipe)  
* et 3 personnes dans le petit local (uniquement les membres d'équipes).  

Par tranche de 1h 20, les locaux doivent être évacués et aérés pendant 10 minutes.  

Les agendas doivent être organisés afin que personne ne soit présent plus de 4 heures en continu. 

### Organisation du travail

#### Accueil des usagers 

L'accueil des usagers doit se faire exclusivement par rendez-vous.  

Toutefois, un rendez-vous peut-être programmé immédiatement si l'accueil est vacant.  

Les usagers doivent se présenter seuls ou accompagnés d'une seule personne.  

Les usagers doivent rester dans la zone d'accueil.  

Les usagers doivent conserver le masque et utiliser du gel hydro-alcoolique.  

#### Télé-travail

Le recours au télé-travail est privilégié et favorisé. L'association peut vous aider à équiper votre domicile pour vous permettre de travailler dans de bonnes conditions. 

#### Horaires de travail

##### Allégement des horaires

Les horaires de travail sont allégés en moyenne de 20% par rapport au temps prévu sur le contrat. Un salarié à temps plein devra travailler 28,5 heures (soit 19 créneaux, voit [plus bas](#decoupage-des-creneaux)), un membre de l'équipe alternant ou volontaire en service civique devra travailler 15 heures par semaine (soit 10 créneaux)

##### Découpage des créneaux

L'organisation du planning prévoit une rotation des équipes qui sont présents au Garage Numérique sur certains créneaux. 

Les horaires sont découpés du lundi au samedi de la manière suivante :  

| créneau | début | fin |
|----------|------|------|
| **travail** | **09h00** | **10h20** |
| pause | 10h20 | 10h30 | 
| **travail** | **10h30** | **11h50** |
| pause | 11h50 | 12h00 | 
| **travail** | **12h00** | **13h20** |
| pause | 13h20 | 13h30 | 
| **travail** | **13h30** | **14h50** |
| pause | 14h50 | 15h00 |
| **travail** | **15h00** | **16h20** |
| pause | 16h20 | 16h30 |
| **travail** | **16h30** | **17h50** |
| pause | 17h50 | 18h00 |
| **travail** | **18h00** | **19h20** |


### Exemple de planning à temps plein

Les cases en italique sont du télétravail, si c'est possible pour le salarié.

#### Technicien à temps plein

| Horaires      | Lundi     | Mardi         | Mercredi      | Jeudi         | Vendredi      |
|---------------|-----------|---------------|---------------|---------------|---------------|
| 9h - 10h20    | Ménage    |               |               | *dev*         | *formation*   |
| 10h30 - 11h50 | Accueil   | Réparations   |               | *dev*         | *divers*      
| 12h - 13h20   |           |               | *divers*      | *dev*         |
| 13h30 - 14h50 |           | Réparations   |               |               |
| 15h - 16h20   | *dev*     | Réparations   | Réparations   | accueil       |
| 16h30 - 17h50 | *dev*     |               | Réparations   | accueil       |
| 18h - 19h20   |           |               | Réparations   | accueil       |

#### Agent comptable à mi-temps

| Horaires      | Lundi     | Mardi         | Mercredi      | Jeudi         | Vendredi      |
|---------------|-----------|---------------|---------------|---------------|---------------|
| 9h - 10h20    |  *compta* | Ménage        |               |               |               |
| 10h30 - 11h50 |  *compta* | Accueil       | démarchage    | *formation*      
| 12h - 13h20   |           |               | démarchage    | gestion       |
| 13h30 - 14h50 |           |               |               |               |
| 15h - 16h20   |           | *compta*      |               |               |        |
| 16h30 - 17h50 |           | *compta*      |               |               |  
| 18h - 19h20   |           |               |               |               |  

