# Introduction

Python est un language de programmation créé en 1991.
La version 3, actuellement utilisée dans la plupart des projets, est parue en 2008. Elle n'est pas compatible avec les anciennes versions de Python.  

Python est un language **interprété**, c'est-à-dire qu'il n'y a pas besoin de le compiler avant de l'exécuter, on peut donc travailler avec dans une **console interactive**.

Python est un language **orienté-objet**, c'est-à-dire (selon [Wikipedia](https://fr.wikipedia.org/wiki/Python_(langage))) :
> Tous les types de base, les fonctions, les instances de classes (les objets « classiques » des langages C++ et Java) et les classes elles-mêmes (qui sont des instances de méta-classes) sont des objets.  

Python est conçu pour avoir une syntaxe lisible:
- les commentaires sont indiquées par le `#`
- les blocs sont identifiés par l'indentation (4 ++space++)

