# Configurer l'espace de travail

## Installer python

Python est peut-être déjà installé sur votre système, mais il faut vérifier la version:

```
user@host:~$ python --version
Python 3.7.3
```

et la liste des versions disponibles:

```
user@host:~$ update-alternatives --list python
/usr/bin/python2
/usr/bin/python3
```

Pour installer la version qui vous manque:
```
sudo apt install python2
# OU
sudo apt install python3
```

Nous avons besoin du numéro de version pour python2 et python3:
```
user@host:~$ ls -l /usr/bin/python{2,3}
lrwxrwxrwx 1 root root 9 mars   4  2019 /usr/bin/python2 -> python2.7
lrwxrwxrwx 1 root root 9 mars  26  2019 /usr/bin/python3 -> python3.7
```

Nous allons donner la priorité 1 à python2 et la priorité 2 à python3:

```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1
```

---

## Installer pip

pip (Package installer for python or "Pip Installs Packages") est un outil très utile pour gérer les dépendances dans un projet python.

Là aussi, il y a pip pou python2 et pip pour python3, mais Debian utilisera la version correpondante à la version de python définie avec `update-alternatives`.

```
user@host:~$  sudo apt install python3-pip python-pip
user@host:~$  pip --version
pip 18.1 from /usr/lib/python3/dist-packages/pip (python 3.7)
```

---

## Utiliser la console python

Pour découvrir python, rien de plus simple, on saisit la commande `python` dans le terminal:

```
user@host:~$ python
Python 3.7.3 (default, Dec 20 2019, 18:57:59) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```
Les trois chevrons `>>>` vous indiquent que vous êtes dans la console python.  
Faites ++ctrl+d++ pour en sortir.

Vous pouvez déclarer une variable, la transformer et l'afficher:

```
>>> MA_VARIABLE = " ceci est le contenu de ma variable "
>>>
>>> print(MA_VARIABLE)
 ceci est le contenu de ma variable 
>>> 
>>> VAR_2 = MA_VARIABLE + " et celle-ci est plus longue"
>>> 
>>> print(VAR_2)
 ceci est le contenu de ma variable  et celle-ci est plus longue
```


