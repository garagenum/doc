# Procédures administratives

Cette rubrique répertorie différentes procédures pour réaliser le travail administratif du Garage Numérique.

- [Recadrage des avis de paiement de l'asp](cadrage-asp)
- [Procédures de compta avec odoo](process-odoo)
- [Procédures des Ressources Humaines](rh)
- [Procédures comptables](compta)
- [Procédures d'installation Windows](windows)