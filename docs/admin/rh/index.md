# Procédures RH

La rubrique suivante contient des guides pour les démarches administratives liées aux Ressources humaines:  

- [Saisir un arrêt de travail](arret-travail.md)
- [Création d'un employé dans odoo](creation-employe.md)
- [Remplissage des bordereaux Uniformation](bordereau-uniformation.md)