# SSH reminder

## Remove a key from known hosts:

    ssh-keygen -f "/home/user/.ssh/known_hosts" -R "key_name"
    
## Create a key:

    ssh-keygen -o -a 100 -t ed25519 -C user@email #create an ED25519 key
    ssh-keygen -b 4096 -t rsa -C user@email #create a RSA key
    ssh-add newkey #add newkey to your SSH agent
    ssh-add *
    ssh-add .
    
## List existing keys:

    ssh-add -l
    
## Display a key:

    cat /home/user/.ssh/key_name
    
## Display a key fingerprint (with random art image):

    ssh-keygen -lvf key_name

## Enable SSH agent:

    eval $(ssh-agent -s)

## Copy a key to a distant server:

    ssh-copy-id user@server #Copy all keys that are registered in SSH agent
    ssh-copy-id -i newkey.pub user@server\n #Copy newkey.pub
    
## Remove a key:

Removing a key from a server can be done by editing your `/home/user/.ssh/authorized_keys` file.
    
## Connect and copy from a different port number:

    ssh -p port user@server
    scp -P port file user@server:way/
    rsync -e ssh -avz /source server:/target
    rsync -ave ssh server:/source /target
    ssh-copy-id -i newkey.pub -p port user@server

## Connect a distant server in your file browser under Linux using SFTP:

Type the adress:

    ssh://user@server:port
    
*See also:* [Enabling a SSH key on a Synology NAS running DSM 6](https://gitlab.com/sakura-lain/ssh-synology-nas-dsm-6/)
