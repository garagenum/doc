# Console postgres

## List databases

```
postgres=# \l
```

## Switch database
```
postgres=# \c db_name
```
List tables
```
db_name=# \dt
```

Show table content
```
db_name=# \d table_name
```

Modify table
```
UPDATE table_name
SET column1 = 'value1',
    column2 = 'value2',
    ...
WHERE condition;
```
