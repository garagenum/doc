# Astuces du Garage

Cette rubrique regroupe des astuces internes au Garage

- [Imprimantes](install-printers.md)
- [Workadventure map du garage](wa.md)
- [Contribuer à la doc avec mkdocs](mkdocs.md)
