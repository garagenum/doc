# Screen

1.Installation  

    sudo apt install screen  

2.Pour démarrer une nouvelle session:  

    screen -S nom-de-la-session  

3.Pour laisser la session en arrière plan  

++ctrl+a+d++  

4.Pour lister les sessions existantes:  

    screen -ls   

5.Pour se reconnecter :  

    screen -r nom-de-la-session   

**See [https://linuxize.com/post/how-to-use-linux-screen/](https://linuxize.com/post/how-to-use-linux-screen/)**

** Pour splitter l'écran: [https://tomlee.co/2011/10/gnu-screen-splitting/](https://tomlee.co/2011/10/gnu-screen-splitting/)**