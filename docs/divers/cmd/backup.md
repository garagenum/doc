# Sauvegarder des données

1. Lister les partitions: 
    ```
    lsblk
    ```
2. Identifier les partitions adéquates : les plus grandes
3. Monter les partitions
    ```
    mkdir /mnt/nom_du_dossier
    mount /dev/sdXy /mnt/nom_du_dossier
    ```
4. Cibler le dossier utilisateur de Windows
    ```
    ls /mnt/nom_du_dossier
    ```
5. Copier le dossier utilisateur vers le disque dur sur le PXE
    ```bash
    rsync -ah /mnt/part1/Users/sandra bellinuxien@192.168.1.75:/home/bellinuxien/BACKUP/num_ordi-nom_personne/ --info=progress2 && sync
    ```
