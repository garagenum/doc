# Heredoc

Heredoc permet d'envoyer du contenu multi-lignes, que ce soit sur la sortie du terminal ou dans une commande.

On utilise un **Here Tag**, qui est souvent `EOF`mais que vous pouvez nommer comme vous voulez.  

#### Multi-lignes SANS Heredoc (avec `\n`)

```
text=$'premièreligne\ndeuxièmeligne'
```

#### Utilisation dans une variable
```
visiteur@domani:~$ MA_VARIABLE=$(cat <<EOF
premiere ligne
deuxième ligne
EOF
)
visiteur@domani:~$ echo "$MA_VARIABLE" 
premiere ligne
deuxième ligne
```

#### Utilisation pour écrire dans un fichier
```
visiteur@domani:~$ cat <<EOF1 > file.txt
premiere ligne
deuxième ligne
EOF1
```