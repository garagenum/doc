# Monter une partition


## Repérer les partitions disponibles

Quelques commandes sont utiles pour connaître l'état des disques, des partitions et des points de montage
```
sudo fdisk -l
sudo df -H
```

Le fichier `/etc/mtab` permet de connaître la liste des partitions montées, leur point de montage et les options

Le fichier `/etc/fstab` définit le montage de partitions au démarrage du système

## Avec udiskctl

On utilise la commande udiskctl qui nous permet de monter un disque externe ou une partition en ligne de commandes, de la même façon que le fait l'environnement de bureau.

### Installation

```
sudo apt install udisks2
```

### Usage

#### Monter une partition
```
udiskct --mount -b /dev/sdb1
```

#### Démonter une partition
```
udiskctl unmount -b /dev/sdb1
```

#### Éjecter proprement une clé usb
```
udiskctl power-off -b /dev/sdb
```

## Avec mount

Avec la commande mount, il est nécessaire de bien définir les options de montage

```
sudo mkdir -p /mnt/cle
sudo mount /dev/sdb1 /mnt/cle
```

Pour démonter une clé
```
sudo umount /mnt/cle
```

