# Wiper un disque (NWIPE)

## DEFINITION

nwipe est un programme qui efface en toute sécurité le contenu entier des disques. Il peut effacer un seul disque ou plusieurs disques simultanément. Il peut fonctionner à la fois comme un outil en ligne de commande sans interface graphique ou avec une interface graphique ncurses.

A l'origine, nwipe est un fork de la commande dwipe utilisée à l'origine par Darik's Boot and Nuke (DBAN). nwipe a été créé à partir d'un besoin d'exécuter la commande dwipe de DBAN en dehors de DBAN, afin de permettre son utilisation avec n'importe quelle distribution hôte, offrant ainsi un meilleur support matériel.

## INSTALLATION

```bash
sudo apt install nwipe
```

![STEP-1](nwipe1.png)

## AFFICHER LES INFORMATIONS DISQUES

Avant d'utiliser nwipe, il est important d'identifier quel disque nous allons effacer, pour ce faire entrez la commande ci-dessous:
```bash
lsblk
```
Cette commande permet d'obtenir la liste et les caractéristiques des disques et de leurs partitions. (La commande ne nécessite pas les droits administrateurs pour être exécutée).

<!-- [LSBLK](sblk2.jpg) -->

:warning: veillez bien à démonter les partitions si c'est le cas comme dans l'exemple ci-dessous.
```bash
sudo umount /home/user/DISK
```

:warning: Attention à ne pas sélecttionner le disque système sur lequel Linux tourne!

## UTILISATION

Pour ouvir Nwipe, utilisez la commande ci-dessous dans le terminal
```bash
sudo nwipe
```

## STEP 1

Appuyez sur la barre d'espace pour sélectionner le lecteur que vous souhaitez effacer.

![STEP-1](nwipe2.png)

# STEP 2

Appuyez sur la touche M pour modifier la méthode que vous souhaitez utiliser. Appuyez sur l'espace pour sélectionner la méthode.

![STEP-3](nwipe3.png)

# STEP 3

Appuyez sur les touches Ctrl+S pour lancer le processus de nettoyage. La durée d'exécution restante et d'autres statistiques peuvent être contrôlées dans la colonne Statistiques.

![STEP-3](nwipe4.png)
