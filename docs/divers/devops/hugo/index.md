# Guides pour Hugo

Ce guide vous permet de développer un site web avec Hugo.

Vous réaliserez un site **from scratch** en suivant ces étapes :  

  - [Installer Hugo](install_hugo.md)  
  - [Créer le site web en local](create_website.md)  
  - [Déployer le site web](deploy_website.md)  
  - [Ajouter un menu et une page](add_menu.md)   
  - [Ajouter des sections](add_sections.md)   
  - [Ajouter une collection d'articles](add_list.md)  

Pour un déploiement en production, vous pouvez mettre en place un workflow complet avec dokku :  
  - [Workflow de production avec Dokku](dokku_workflow.md)  

D'autres ressources pour aller plus loin :  
  - [Course on mikedane.com](https://www.mikedane.com/static-site-generators/hugo/)  
  - [Official doc](https://gohugo.io/documentation/) 