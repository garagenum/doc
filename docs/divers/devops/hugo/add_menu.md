# Ajouter un menu et une page de contenu

Pour commencer à entrer du contenu sur notre site, nous allons intégrer un menu dans le header et créer une nouvelle page :   
  - [création du menu](#creer-un-menu)
  - [créer une page](#creer-une-page)

## Créer un menu

Nous créons un partial pour définir le menu, et nous appelons le partial dans `header.html`, jsute après la balise </a>.  

### Appel du partial menu.html

!!! note "Appel du partial dans header.html"
    ```html
    {\{ partial "menu.html" . }}
    ```

### Création du partial menu.html
??? note "layouts/partials/menu.html"
    === "Contenu"
        ```html
        <nav class="menu" aria-label="menu">
            <ul class="menu__items" aria-label="menu">
                {\{ $current := . }}
                {\{ range .Site.Menus.main }}
                <li class="menu__items__item" aria-label="{\{ .Name }}">
                    <a href="{\{ .URL }}" class="menu__items__item__link {\{ if $current.IsMenuCurrent "main" . }}active{\{ end }}"
                        alt="{\{ .Name }}" role="menuitem">
                        {\{ .Name }}
                    </a>
                </li>
                {\{ end }}
            </ul>
        </nav>
        ```
    === "Explication"
        la fonction `range` scanne tous les paramètres pour savoir les pages qui appartiennent au menu `main`

## Créer du contenu

Pour l'exemple, nous choisissons de créer la page `Présentation`. 

### Création du template

Nous allons créer un template
```html
{\{ .Content }}
```