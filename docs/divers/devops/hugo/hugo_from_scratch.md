# Construire un site statique avec Hugo

La création d'un site avec Hugo se fait en plusieurs temps :   
  1. [Installation d'Hugo](install_hugo.md)  
  2. [Préparation de l'environnement de travail](#preparer-lenvironnement-de-travail)  
  3. [Création du site en local](#creation-du-site-web-hugo-en-local)  
  4. [Déploiement local](#deploiement-local)  
  5. [Déploiement en ligne avec Gitlab Pages](#deploiement-en-ligne-avec-gitlab-pages)  


## Préparer l'environnement de travail



### Installer les dépendances 
```
sudo apt install git 
```

### Créer le dossier du projet
    
```
mkdir <name-of-project>
```

### Initialisez le versioning

```
cd <name-of-project>
git init
```

!!! tip "Pro tip"
    À partir de maintenant, vous pouvez utilisez git pour gérer les versions successives du code.  
    Après chaque changement significatif, utilisez la commande `git add` pour ajouter les changements,
    et `git commit` pour les valider.  
    Utilisez une [*CheatSheet*](https://rogerdudler.github.io/git-guide/) pour vous aider à mémoriser les commandes. 

### Création du README

```
echo "#<name-of-project>" > README.md
```
Vous pouvez maintenant modifier ce fichier, qui doit présenter le projet et le fonctionnement du site internet, 
en utilisant le format [Markdown](https://docs.roadiz.io/fr/latest/user/write-in-markdown/)

## Création du site web Hugo en local





```
```
```
```
```
```
```