# Créer un site web avec Hugo

!!! important "**Dans ce tutoriel, vous devez remplacer  `<name-of-project>`,  par le nom de votre site (ex: info2000)**"

## Étape 1: Construire le site 

## Création du site

```
hugo new site my-website
```
Cette commande va créer tout l'arborescence nécessaire à Hugo dans le dossier `./<my-website`.

## Modification du `config.toml`

Edit at least `url` to reflect the adress where it will be deployed by gitlab pages.  


!!!note "Determine URL"
    If your project is located at `https://gitlab.com/myuser/agroup/myproject`, then 
    your website will be deployed at `https://myuser.gitlab.io/agroup/myproject`.

Ajouter les éléments suivants à votre `config.toml`:   
    ```
    baseurl = "https://myuser.gitlab.io/agroup/myproject/"
    title = "My website title"
    author = "Le Garage Numérique"
    [params]
        description = "A description for my website, that can be used on front page."
        favicon = "favicon.ico"
        logo = "my-logo.png"
    ```
and place pictures in `static` folder.


## Étape 2: Construire la home page

### Création de la home page
```
hugo new _index.md
```
A new file is created at `content/_index.md`

### Modification

```
---
title: "Mon super contenu"
date: 2020-12-08T00:13:21+01:00
draft: false
---

Ceci est le contenu de la home page pour mon site web
```
La partie encadrée entre `---` s'appelle le **front matter**, il s'agit de méta-données utilisées pour le déploiement du site.

### Création des templates

Dans le dossier `layouts` on créé l'arborescence suivante:
    ```
    ├── _default
    │   ├── baseof.html
    ├── partials
    |   ├── head.html
    |   ├── header.html
    |   ├── hero.html
    |   ├── styles.html
    └── index.html
    ```


??? note "**layouts/index.html**"   
    === "Contenu"
        ```
        {{define "main"}}
        {{ partial "hero.html" }}        
        {{end}}
        ```
    === "Explication"
        Il s'agit d'un template spécifique à la page d'accueil.   
        On appelle le template `layouts/default/baseof.html` avec la directive `{{define main}}`.   
        On appelle aussi `partials/hero.html`, qui est en quelque sort le slogan de bannière du site.

??? note "**layouts/partials/hero.html**"
    === "Contenu"
        ```
        <main class="hero">
            <div class="hero__caption" >
                {{ .Content }}
            </div>
        </main>
        ```
    === "Explication"
        La directive `{{ .Content }}` récupère le contenu de `content/_index.md` après les balises de l'en-tête Toml.
 
??? note "**layouts/default/baseof.html**"
    === "Contenu"
        ```
        <!DOCTYPE html>
        <html>
            {{- partial "head.html" . -}}

            <body>
                {{- partial "header.html" . -}}
                {{- block "main" . }}{\{- end }}
                {{- partial "footer.html" . -}}
            </body>

        </html>
        ```
    === "Explication"
        On appelle le partial `head.html`, puis on créé le `body` avec les partials `header.html` et `footer.html` .  
        Le bloc central, "main", sera construit à partir de chaque page.

??? note "**layouts/partials/head.html**"
    === "Contenu"
        ```
        <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta name="robots" content="index, follow" />
            <title>{{ $.Site.Title }} - {{ .Title }}</title>
            <link rel="icon" href="{{ $.Site.BaseURL }}{{ $.Site.Params.favicon }}" />
            <meta name="description" content="{{ $.Site.Params.description }}" />

            {{ hugo.Generator }}
            <!-- CSS -->
            {{ partial "styles.html" . }}
        </head>
        ```
    === "Explication"
        C'est ici qu'on placera notamment les informations de référencement **SEO**.  
        Entre double-accolades, on appelle des variables.   
        Quand celles-ci commencent par `$.Site.Params`, il s'agit des variables définies dans la rubrique [params] de `config.toml`
        

??? note "**layouts/partials/header.html**"
    === "Contenu"
        ```
        <header class="header">
            <div class="header__title">
                <a href="{{ "/" | relLangURL }}" class="header__title__link" alt="Home">
                    {{ if $.Site.Params.logo }}
                    <img src="{{ $.Site.BaseURL }}{{ $.Site.Params.logo }}" class="header__title__logo" alt="{{ $.Site.Title }}">
                    {{ else }}
                    {{ $.Site.Title }}
                    {{ end }}
                 </a>
            </div>
        </header>
        ```
    === "Explication"
        Ici des classes CSS sont appellées (`header`, `header__title`, etc.).   
        Elles doivent être définies dans le fichier `assets/css/style.css`

??? note "**layouts/partials/footer.html**"
    ```
    <div class="container content">
    <footer>
    <div>
        <p class="right credit">
        Fancy footer here. Link to <a href="/">Home Page</a>
        </p>
    </div>
    </footer>
    </div>

    </body>
    </html>
    ```

### Le CSS

??? note "**layouts/partials/styles.html**"
    === "Contenu"
        ```
        {{ $style := resources.Get "css/style.css" }}
        {{ $base := resources.Get "css/base.css" }}
        {{ $bundle := slice $style $base  | resources.Concat "css/bundle.css" }}
        {{ $css := $bundle | resources.Minify }}
        <link rel="stylesheet" href="{{ $css.Permalink }}">
    === "Explication"
        On appelle les fichiers `style.css` et `base.css` dans  `assets/css/` et on les minimise, après les avoir concaténés.

??? note "**assets/css/style.css**"
    ```
    /* HEADER */

    .header {
        background-color: var(--white);
        box-shadow: 0 1px 5px var(--divider);
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-template-rows: 1fr;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 999;
    }

    .header__title {
        align-items: center;
        display: flex;
        font-size: 1.1rem;
        justify-content: flex-start;
        margin: 1rem 4.5rem;
    }

    .header__title__link {
        color: var(--primary-dark);
    }

    .header__title__logo {
        position: absolute;
        top: 8px; left: 20px;
        height: 55%;
        width: 13%;
        max-width: 20%;
        vertical-align: middle;
        transition: all .4s ease-in-out;
        margin-bottom: 0px;
        margin-top: 10px; 
        display : inline-block;
        margin-left: 8%;

    /* HERO */
    .hero {
        align-content: center;
        background-attachment: fixed;
        background-position: 100% 20%;
        background-repeat: no-repeat;
        background-size: contain;
        display: flex;
        height: 90vh;
        justify-content: flex-start;
        width: 100%;
    }

    .hero__caption {
        align-items: flex-start;
        animation: focus 0.95s cubic-bezier(0.39, 0.575, 0.565, 1) both;
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin-left: 4.5rem;
    }

    .hero__caption > h1 {
        font-size: var(--title);
    }

    .hero__caption > h2 {
        font-size: var(--subheader);
        margin-top: 0.45rem;
    }

    /* FOOTER */
    .right {
        float: right;
        margin-left: 1rem;
    }
    .credit {
    padding-top: 3rem;
    font-size: small;
    }
    ```

??? note "**assets/css/base.css**"
    ```
    :root {
        /* Font Sizes */
        --title: 3.998rem;
        --subtitle: 2.827rem;
        --header: 1.999rem;
        --subheader: 1.414rem;

        /* Colors */
        --primary: #ffc107;
        --primary-dark: #ffa000;
        --primary-light: #ffecb3;
        --primary-text: #212121;
        --secondary-text: #333333;
        --accent: #536dfe;
        --divider: #bdbdbd;
        --white: #fdfdfd;

        /* Breakpoints */
        --sm: 576px;
        --md: 768px;
        --lg: 992px;
        --xl: 1200px;
    }

    a {
        color: var(--accent);
    }

    a:hover {
        color: var(--primary-dark);
    }

    body {
        color: var(--primary-text);
    }

    figcaption {
        font-size: 0.9rem;
        text-align: center;
    }

    hr {
        color: var(--divider);
        opacity: 0.30;
        width: 25%;
    }

    i {
        font-size: var(--subheader);
    }

    input,
    textarea {
        border: 2px solid var(--divider);
    }

    input:focus,
    textarea:focus {
        border: 2px solid var(--accent);
    }

    pre {
        border: 1px solid var(--divider);
        overflow: auto;
        margin-bottom: 1.75rem;
        padding: 1rem 1.75rem;
        text-align: left;
        width: 100%;
    }

    textarea {
        resize: none;
    }
    ```
