# Connecter gitpod à gitlab

## Activer l'intégration de gitpod

1. Dans votre instance gitlab, cliquez sur votre avatar en haut à droite, puis sur `Settings`
   ![boite de dialogue de gitlab, pour accéder aux réglages utilisateurs](img/gitlab-user-settings.png)

2. Dans le panneau latéral de gauche, cliquez sur `Preferences` puis, tout en bas de cette page, cliquez sur `Activer Gitpod`
   ![Options de gitlab pour activer l'intégration de gitpod](img/activate-gitpod-in-gitlab.png)

3. Clique sur Authorisez dans la fenêtre qui apparaît.
   ![Fenetre d'autorisation de Gitpod dans Gitlab, bouton autoriser et refuser](img/authorize-gitpod.png)

## Commencez à travailler avec gitpod

1. Vous pouvez maintenant lancer Gitpod depuis la page d'accueil de votre projet:
  ![picture showing bouton de démarrage de Gitpod sur la page d'accueil de Gitlab](img/run-gitpod-from-gitlab.png)

2. Au premier démarrage, gitpod va vous demander de créer un compte (authentification unique avec votre compte gitlab):  
   ![picture showing gitpod account creation box](img/create-gitpod-account.png)

3. Il faut attendre que l'espace de travail se déploie (il s'agit d'un déploiement Docker):
   ![image de l'écran d'attente du déploiement de l'espace de travail Gitpod](img/wait-gitpod-workspace.png)

4. Vous êtes maintenant dans votre espace de travail Gitpod, un IDE collaboratif et une plate-forme de déploiement en ligne! 
  ![image d'accueil d'un workspace gitpod](img/gitpod-workspace.png)

5. En cliquant sur le logo ![logo de gitpod](img/gitpod-logo.png), vous êtes redirigés vers une page vous présentant l'ensemble de vos workspaces actifs:
  ![image de la page d'accueil listant les workspaces](img/gitpod-workspaces.png)
