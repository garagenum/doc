# Réinitialiser un mot de passe Windows

## Créer une clé bootable Windows

### Installation du logiciel de création de clé

=== "Sur Windows"
    [Télécharger Rufus](https://github.com/pbatard/rufus/releases/download/v3.11/rufus-3.11.exe)
=== "Sur Linux" 
    ```
    # add-apt-repository ppa:nilarimogard/webupd8
    # apt install woeusb
    ```
    Sur Debian:  
    ```
    $ git clone https://github.com/slacka/WoeUSB && cd WoeUSB 
    $ git checkout obsoleted
    $ ./setup-development-environment.bash
    # apt install devscripts equivs gdebi-core
    $ cd <WoeUSB source tree directory, the folder that contains the `src` folder>
    $ mk-build-deps
    # gdebi woeusb-build-deps_<version>_all.deb
    $ dpkg-buildpackage -uc -b
    # gdebi ../woeusb_<version>_<architecture>.deb
    ```
    
Il ne vous reste plus qu'à créer la clé avec WoeUsb ou Rufus.

### Hack de l'accès à la session utilisateur sous Windows

#### Acceder à l'invite de commande depuis la clé Windows  

1. Booter sur la clé
2. Choisir la langue française
3. En bas à gauche, cliquer sur "Réparer" ou "Repair"
4. Cliquer sur "Troubleshoot" (logo boite à outils)
5. Cliquer sur "Options avancées"
6. Cliquer sur "Command Prompt" (logo du terminal)

#### Remplacer un outil d'ergonomie par le terminal  

1. Identifier la lettre de la partition Windows
        ```
        diskpart
        list volume
        ```
2. Faire un backup du logiciel qu'on va remplacer
        ```
        copy D:\Windows\System32\sethc.exe D:\
        ```
3. Remplacer le logiciel par cmd.exe et quitter
        ```
        copy /y D:\Windows\System32\cmd.exe D:\Windows\System32\sethc.exe
        exit
        ``` 
4. Redémarrer l'ordinateur

#### Créer un nouveau utilisateur avec les options administrateur

1. Arrivé sur Windows, taper 5 fois sur la touche MAJ  
2. Créer un utilisateur  
    net user bellinuxien /add
3. L'ajouter au groupe administrateurs  
    net localgroup administrators bellinuxien /add

#### Changer le mot de passe perdu 

1.  Se loger dans bellinuxien  
2. Démarrer le logiciel netplwiz  
3. Selectionner le compte perdu  
4. Cliquer sur "Reset Password"  

#### Restaurer l'outil ergonomique  

1. Ouvrir cmd
2. Remplacer sethc.exe par l'original 
    copy /y D:\sethc.exe D:\Windows\System32\sethc.exe

#### Supprimer bellinuxien  

1. Se loger dans l'utilisateur restauré  
2. Démarrer le logiciel netplwiz  
3. Cliquer sur "bellinuxien"  
4. Cliquer sur "Remove"  


