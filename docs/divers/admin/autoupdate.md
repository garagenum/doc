# Mises à jour automatiques

## Utilisation de cron-apt

### Installation du logiciel

```
sudo apt update
sudo apt install cron-apt
```
Si l'installateur vous demande de configurer le serveur Postfix (c'est le serveur chargé d'envoyer des messages à l'administrateur), vous n'avez qu'à choisir, "pas de configuration", ou "Local Uniquement" pour que les messages soient envoyés dans `/var/mail/$USER`, ou "Site Internet" pour utiliser le serveur SMTP de votre messagerie (gmail, etc.)

### Configuration 

!!! note "/etc/cron-apt/config"
    ```
    APTCOMMAND=/usr/bin/apt
    OPTIONS="-o quiet=1 Dir::Etc::SourceList=/etc/apt/sources.list"
    ```

!!! note "/etc/cron-apt/action.d/3-download"
    ```
    autoclean -y
    full-upgrade -y -o APT::Get::Show-Upgraded=true
    ```

Comme indiqué dans `/etc/cron.d/cron-apt`, la tâche apt-cron se déclenche tous les jours à 4h du matin.

### Re-démarrage du service
```
sudo systemctl restart cron.service
```

## Avec Unattended-Upgrade

### Installation des dépendances

```
sudo apt install unattended-upgrades apt-listchanges
```

### Configuration

!!! note "/etc/apt/apt.conf.d/50unattended-upgrades"
    Modifier l'élément `Unattended-Upgrade` de la manière suivante:  
    ```
    Unattended-Upgrade::Origins-Pattern {
        "origin=*";
    };
    ```

!!! note "/etc/apt.conf.d/02periodic"
    ```
    // Control parameters for cron jobs by /etc/cron.daily/apt-compat //


    // Enable the update/upgrade script (0=disable)
    APT::Periodic::Enable "1";


    // Do "apt-get update" automatically every n-days (0=disable)
    APT::Periodic::Update-Package-Lists "1";


    // Do "apt-get upgrade --download-only" every n-days (0=disable)
    APT::Periodic:Download-Upgradeable-Packages "1";


    // Run the "unattended-upgrade" security upgrade script
    // every n-days (0=disabled)
    // Requires the package "unattended-upgrades" and will write
    // a log in /var/log/unattended-upgrades
    APT:Periodic::Unattended-Upgrade "1";


    // Do "apt-get autoclean" every n-days (0=disable)
    APT:Periodic::AutocleanInterval "21";


    // Send report mail to root
    //     0:  no report             (or null string)
    //     1:  progress report       (actually any string)
    //     2:  + command outputs     (remove -qq, remove 2>/dev/null, add -d)
    //     3:  + trace on
    APT:Periodic::Verbose "1";
    ```

### Plannifier l'exécution de `unattended-upgrades`

On utilise un éditeur intégré avec la commande `sudo systemctl edit apt-daily.timer`

!!! note "apt-daily.timer"
    ```bash
    [Unit]
    Description=Daily apt download activities

    [Timer]
    OnCalendar= *-*-* 05:00
    #RandomizedDelaySec=12h
    Persistent=true

    [Install]
    WantedBy=timers.target
    ```