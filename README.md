# Introduction

Ce projet contient la documentation du Garage Numérique, hébergé par Gitlab Pages grâce au logiciel MkDocs.

## Travailler en local

Pour participer à la documentation et tester vos modifications avant de les mettre en ligne, il vous faut installer mkdocs.

Utilisez pour cela un environnement virtuel:
```
virtualenv venv
source venv/bin/activate
```

Vous pouvez maintenant utiliser pip pour installer mkdocs:

```
pip install mkdocs
pip install -r requirements.txt
mkdocs serve
```  

Open up `http://127.0.0.1:8000/` in your browser.  

## Ajouter du contenu

Chaque page de contenu correspond à un fichier markdown (.md)  

La documentation est divisée en sections, et en sous-sections.  

Pour chaque section / sous-section, il y a un fichier index.md pour lister et mettre des liens vers les différents contenus  de la section.  

Il faut aussi penser à ajouter le lien vers le nouveau fichier de contenu dans la partie `nav` de `mkdocs.yaml`   

## Astuces de mise en page  

### Insérer du code  

Utilisez la suite de caractères suivants ` ``` `  pour encadrer votre code:  

```  
#```  
Ceci est du   
code   
#```  
```   

Utilisez un seul **`** pour surligner un mot ou une phrase comme du code   

### Insérer une note  

Utilisez la syntaxe suivante   

```  
!!! note "Lisez bien cette note"   
    En indentant vos lignes à partir de cet tag `!!! note`,  
    vous obtenez un encadré dans le texte.   
```   

Plus d'informations sur `https://squidfunk.github.io/mkdocs-material/reference/admonitions/`    

### Insérer un groupe avec des tabulations

```
=== "Python"
    ```
    print(something)
    ```

=== "Bash"
    ```
    echo something
    ```
```