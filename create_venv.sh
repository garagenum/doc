#!/bin/bash

# Read configurations from pyvenv.cfg
home=$(awk -F' = ' '$1=="home"{print $2}' pyvenv.cfg)
executable=$(awk -F' = ' '$1=="executable"{print $2}' pyvenv.cfg)
version=$(awk -F' = ' '$1=="version"{print $2}' pyvenv.cfg)

# Check if venv exists, if not, create it using the details from pyenv.cfg
if [ ! -d "venv" ]; then
    $executable -m venv venv
fi

# Install requirements
venv/bin/python -m pip install --upgrade pip
venv/bin/python -m pip install -r requirements.txt
